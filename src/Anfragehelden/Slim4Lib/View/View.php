<?php declare(strict_types=1);

namespace Anfragehelden\Slim4Lib\View;

use Exception;
use RuntimeException;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * View - Render PHP view scripts into a PSR-7 Response object
 */
class View
{

    const HEADER_TYPE_JS = 'js';
    const HEADER_TYPE_CSS = 'css';

    /**
     * @var string
     */
    protected string $layoutFile;

    /**
     * @var array
     */
    protected array $attributes = [];

    /**
     * @var string
     */
    protected string $content;

    /**
     * @var array
     */
    protected array $header = [self::HEADER_TYPE_CSS => [], self::HEADER_TYPE_JS => []];

    /**
     * @var array
     */
    protected array $appendHeader = [self::HEADER_TYPE_CSS => [], self::HEADER_TYPE_JS => []];

    /**
     * @var string
     */
    protected string $template;

    /**
     * @var bool
     */
    protected bool $customTemplate = false;

    /**
     * @var boolean
     */
    protected bool $terminal = false;

    /**
     * @var boolean
     */
    protected bool $rendered = false;

    /**
     * @var array
     */
    protected array $mca = array();

    /**
     * @param string $layoutFile
     * @param array  $attributes [optional]
     */
    public function __construct(string $layoutFile, array $attributes = [])
    {
        $this->setLayoutFilePath($layoutFile);
        $this->assign($attributes);
    }

    /**
     * @param string      $type
     * @param string      $filename
     * @param bool        $mtime
     * @param int|null    $offset
     * @param bool|null   $localFile
     * @param string|null $attributes
     *
     * @return $this
     * @throws Exception
     */
    public function addHeader(string $type, string $filename, bool $mtime = true, ?int $offset = null, ?bool $localFile = true, ?string $attributes = null): self
    {
        $values = array(
            'filename' => $filename,
            'mtime' => $mtime,
            'localFile' => $localFile,
            'attributes' => $attributes,
        );

        if ($offset === null) {
            $this->header[$type][] = $values;
        } else {
            if (isset($this->appendHeader[$type][$offset]) || isset($this->header[$type][$offset])) {
                throw new Exception('Header offset ' . $offset . ' exists!');
            }
            $this->appendHeader[$type][$offset] = $values;
        }
        return $this;
    }

    /**
     * Get a (js/css) header
     *
     * @param string $type
     *
     * @return array
     */
    public function getHeader(string $type): array
    {
        ksort($this->appendHeader[$type]);
        return $this->header[$type] + $this->appendHeader[$type];
    }

    /**
     * Render a template
     *
     * $data cannot contain template as a key
     *
     * throws RuntimeException if $template does not exist
     *
     * @param Response $response
     * @param array    $data
     *
     * @return Response
     *
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function render(Response $response, array $data = []): Response
    {
        $this->setProperties($data);

        if (!$this->isTerminal() && !$this->customTemplate) {
            $this->setTemplate($this->getTemplateByCalledUri());
        }

        $this->content = $this->fetch($this->template);

        if (!$this->isTerminal()) {
            $output = $this->fetch($this->layoutFile);
            $response->getBody()->write($output);
        } else {
            $response->getBody()->write($this->content);
        }

        $this->rendered = true;

        return $response;
    }

    /**
     * Set the template
     *
     * @param string $template
     *
     * @return $this
     */
    public function setTemplate(string $template): self
    {
        $this->template = $template;
        return $this;
    }

    public function setCustomTemplate(string $template): self
    {
        $this->customTemplate = true;
        $this->template = $template;
        return $this;
    }

    /**
     * Get an array with the module, controller, action by requested uri
     *
     * @return array
     */
    public function getModuleControllerAction(): array
    {
        if (empty($this->mca)) {
            $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
            $parts = explode('/', $url);

            $module = 'Application';
            $controller = (isset($parts[2]) && $parts[2] != '') ? $parts[2] : 'index';
            $action = (isset($parts[3]) && $parts[3] != '') ? $parts[3] : 'index';

            if (isset($parts[1]) && $parts[1] != '') {
                $module = '';
                foreach (explode('-', $parts[1]) as $prop => $value) {
                    $module .= ucfirst($value);
                }
            }

            $this->mca = array(
                'module' => $module,
                'controller' => $controller,
                'action' => $action,
            );
        }
        return $this->mca;
    }

    /**
     * Get template path by requested uri
     *
     * @return string
     */
    public function getTemplateByCalledUri(): string
    {
        $mca = $this->getModuleControllerAction();
        return 'app/modules/' . $mca['module'] . '/views/' . $mca['controller'] . '/' . $mca['action'] . '.phtml';
    }

    /**
     * Get the attributes for the renderer
     *
     * @return array
     */
    public function getAssignedValues(): array
    {
        return $this->attributes;
    }

    /**
     * Set the attributes for the renderer
     *
     * @param array $attributes
     */
    public function assign(array $attributes): void
    {
        foreach ($attributes as $key => $value) {
            $this->attributes[$key] = $value;
        }
    }

    /**
     * Set the layout file path
     *
     * @param string $layoutFile
     *
     * @return $this
     */
    public function setLayoutFilePath(string $layoutFile): self
    {
        $this->layoutFile = $layoutFile;
        return $this;
    }

    /**
     * Get the layout file path
     *
     * @return string
     */
    public function getLayoutFilePath(): string
    {
        return $this->layoutFile;
    }

    /**
     * If terminal true the default layout will not be used
     *
     * @param boolean $terminal
     *
     * @return $this
     */
    public function setTerminal(bool $terminal): self
    {
        $this->terminal = $terminal;
        return $this;
    }

    /**
     * Checks is terminal mode
     *
     * @return boolean
     */
    public function isTerminal(): bool
    {
        return $this->terminal;
    }

    /**
     * Checks view is rendered
     *
     * @return boolean
     */
    public function isRendered(): bool
    {
        return $this->rendered;
    }

    /**
     * Renders a template and returns the result as a string
     *
     * cannot contain template as a key
     *
     * throws RuntimeException if $template does not exist
     *
     * @param string $template
     *
     * @return mixed
     *
     */
    public function fetch(string $template): string
    {
        if (!is_file($template)) {
            throw new RuntimeException("View cannot render `$template` because the template does not exist");
        }
        ob_start();
        $this->protectedIncludeScope($template);
        return ob_get_clean();
    }

    /**
     * @param string $template
     */
    protected function protectedIncludeScope(string $template): void
    {
        include $template;
    }

    /**
     * Set properties to use in the view
     *
     * @param array $data
     *
     * @throws InvalidArgumentException
     */
    protected function setProperties(array $data): void
    {
        if (isset($data['template'])) {
            throw new InvalidArgumentException("Duplicate template key found");
        }

        foreach (array_merge($this->attributes, $data) as $key => $value) {
            if (property_exists($this, $key)) {
                throw new InvalidArgumentException("Duplicate class property. Do not use already declared class properties in data array");
            }

            $this->{$key} = $value;
        }
    }

    /**
     * Render a partial view
     *
     * @param string $partialPath View Skript
     * @param array  $data        View Daten
     *
     * @return string
     */
    public function partial(string $partialPath, array $data = []): string
    {
        ob_start();
        extract($data);
        $this->protectedIncludeScope($partialPath);
        return ob_get_clean();
    }

    /**
     * @param string $string
     *
     * @return string
     */
    public function escape(string $string): string
    {
        return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
    }
}
